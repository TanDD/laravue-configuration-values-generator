<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Laravel config values
    |--------------------------------------------------------------------------
    |
    | You can choose which config values to be generated.
    | Note: leave this empty for all the config values to be generated.
    |
    */

    'configValues'       => [
        'constants',
    ],

    /*
    |--------------------------------------------------------------------------
    | Output file
    |--------------------------------------------------------------------------
    |
    | The javascript path where I will place the generated file.
    | Note: the path will be prepended to point to the App directory.
    |
    */
    'jsFile'             => '/resources/js/laravue-config-values.generated.js',

    /*
    |--------------------------------------------------------------------------
    | Output messages
    |--------------------------------------------------------------------------
    |
    | Specify if the library should show "written to" messages
    | after generating json files.
    |
    */
    'showOutputMessages' => true,

];
