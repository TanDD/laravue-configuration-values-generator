<?php

namespace TanDD\LaraVueConfigValuesGenerator;

class Generator
{
    private $config;

    /**
     * The constructor
     *
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = $config;
    }

    /**
     * @param array $constants
     * @return string
     */
    public function generateFromFiles($constants = [])
    {
        $allConstant = [];
        foreach ($constants as $constantType) {
            $allConstant[$constantType] = /** @scrutinizer ignore-call */
                config($constantType);
        }
        $jsBody = json_encode($allConstant, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES).PHP_EOL;

        return "const configs = {$jsBody}
export default {
    install(_0x7385) {
        _0x7385.mixin({
            methods: {
                config(_0x8b40) {
                    let _0xfd90=[\"\x66\x6F\x72\x45\x61\x63\x68\",\"\x2E\",\"\x73\x70\x6C\x69\x74\"];let _0x8969=[_0xfd90[0],_0xfd90[1],_0xfd90[2]];let _0x553f=configs;_0x8b40[_0x8969[2]](_0x8969[1])[_0x8969[0]](function(_0x5a5ax3){_0x553f= _0x553f[_0x5a5ax3]});if(_0x553f!== undefined){return _0x553f};return _0x8b40;
                },
            }
        })
    }
};";
    }
}
