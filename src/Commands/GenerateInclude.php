<?php namespace TanDD\LaraVueConfigValuesGenerator\Commands;

use Illuminate\Console\Command;

use TanDD\LaraVueConfigValuesGenerator\Generator;

class GenerateInclude extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravue-config-values:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates a Vue Configuration Values compatible js array out of project";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $constantFiles = /** @scrutinizer ignore-call */
            config('laravue-config-values-generator.configValues');
        $config        = /** @scrutinizer ignore-call */
            config('laravue-config-values-generator');
        $jsFile        = $this->_getFileName();
        $data          = (new Generator($config))->generateFromFiles($constantFiles);
        file_put_contents($jsFile, $data);
        if ($config['showOutputMessages']) {
            $this->info('Written to : '.$jsFile);
        }
    }

    /**
     * @param string $fileNameOption
     * @return string
     */
    private function _getFileName($fileNameOption = null)
    {
        if (! is_null($fileNameOption)) {
            return /** @scrutinizer ignore-call */ base_path().$fileNameOption;
        }

        return /** @scrutinizer ignore-call */ base_path()./** @scrutinizer ignore-call */ config('laravue-config-values-generator.jsFile');
    }
}
