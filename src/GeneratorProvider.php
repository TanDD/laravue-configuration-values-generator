<?php

namespace TanDD\LaraVueConfigValuesGenerator;

use Illuminate\Support\ServiceProvider;

class GeneratorProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton('laravue-config-values.generate', function () {
            return new Commands\GenerateInclude;
        });

        $this->commands('laravue-config-values.generate');
        $this->publishes([
            __DIR__.'/config/laravue-config-values-generator.php' => /** @scrutinizer ignore-call */ config_path('laravue-config-values-generator.php'),
        ]);
        $this->mergeConfigFrom(__DIR__.'/config/laravue-config-values-generator.php', 'laravue-config-values-generator');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laravue-config-values-generator'];
    }
}
