<h1 align="center">LaraVue Config Values</h1>

<p align="center">
<a href="https://packagist.org/packages/tandd/laravue-configuration-values">
<img src="https://scrutinizer-ci.com/gl/tandd/Tandd/laravue-configuration-values-generator/badges/quality-score.png?b=master&s=cfbdfce7fa4dbc77fc66d98cadfb0bba72f8ebfd" alt="Scrutinizer Code Quality"></a>
<a href="https://scrutinizer-ci.com/gl/tandd/Tandd/laravue-configuration-values-generator/build-status/master"><img src="https://scrutinizer-ci.com/gl/tandd/Tandd/laravue-configuration-values-generator/badges/build.png?b=master&s=bb95bf9f04617342ac65ddf6db29e3f47cd13f67" alt="Build Status"></a>
<a href="https://packagist.org/packages/tandd/laravue-configuration-values"><img src="https://scrutinizer-ci.com/gl/tandd/Tandd/laravue-configuration-values-generator/badges/code-intelligence.svg?b=master&s=47141bf474e327f27394736b217605789eed03c3" alt="Total Downloads"></a>
</p>

## About

Laravel 5 package that allows you to share your [Laravel Configuration Values](https://laravel.com/docs/7.x/configuration#accessing-configuration-values)
with your [vue](http://vuejs.org/) front-end, using [laravue-config-values](https://gitlab.com/TanDD/laravue-configuration-values-generator).


## Laravel 5.7 notice!

Configuration paths have changed in Laravel 5.7, in order for this package to function properly you need to configure correct paths for jsFile in your `config\laravue-config-values-generator.php`.


## Install the package

In your project:

```
composer require tandd/laravue-configuration-values --dev
```

### For Laravel 5.4 and below:
For older versions of the framework:

Register the service provider in ``config/app.php``

```php
TanDD\LaraVueConfigValuesGenerator\GeneratorProvider::class,
```

Next, publish the package default config:

```
php artisan vendor:publish --provider="TanDD\LaraVueConfigValuesGenerator\GeneratorProvider"
```

Then add the config value you want to generator in ``config/laravue-config-values-generator.php``

```php
return [
    'configValues'          => [
            'constants',
             ...
        ],
];
```
You can generate all config in ``config`` folder.

## Using laravue-config-values in VueJS

Generate the include file with
```
php artisan laravue-config-values:generate
```

Then defined in Vue main.js file:
```VueJs
import Vue from 'vue';

import ConfigValues from './laravue-config-values.generated';

Vue.use(ConfigValues);
```

### Using in template

The generator adjusts the strings in order to work with laravue-config-values named formatting,
so you can reuse your Laravel config with parameters.

config/constants.php:
```php
return [
    'MAX_AGE' => 50,
];
```

in laravue-config-values.generated.js:
```js
    "constants":{
        "MAX_AGE": 50
    } 
```

Blade template:
```html
<div class="age">
    <p>{{ config('constants.MAX_AGE') }}</p>
</div>
```

Vue template:
```vue
<div class="age">
    <p>{{ config('constants.MAX_AGE') }}</p>
</div>
```
Vue script:

```javascript
console.log(this.config('constants.MAX_AGE'))
```


# License

Under [MIT](LICENSE)


# Auth

[Tandd](mailto:tandd.dev@gmail.com)
